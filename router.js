import Vue from 'vue'
import Router from 'vue-router'
import { scrollBehavior } from '~/utils'

Vue.use(Router)

const Home = () => import('~/pages/home').then(m => m.default || m)
const Index = () => import('~/pages/index').then(m => m.default || m)
const Products = () => import('~/pages/product/index').then(m => m.default || m)
const Product = () => import('~/pages/product/show').then(m => m.default || m)
const Order = () => import('~/pages/order').then(m => m.default || m)

const Admin = () => import('~/pages/auth/admin/index').then(m => m.default || m)
const AdminProducts = () => import('~/pages/auth/admin/products').then(m => m.default || m)
const AdminProduct = () => import('~/pages/auth/admin/product/show').then(m => m.default || m)
const AdminCreateProduct = () => import('~/pages/auth/admin/product/create').then(m => m.default || m)
const AdminEditProduct = () => import('~/pages/auth/admin/product/edit').then(m => m.default || m)
const AdminOrders = () => import('~/pages/auth/admin/order/index').then(m => m.default || m)

const Login = () => import('~/pages/auth/login').then(m => m.default || m)
const Register = () => import('~/pages/auth/register').then(m => m.default || m)
const PasswordReset = () => import('~/pages/auth/password/reset').then(m => m.default || m)
const PasswordRequest = () => import('~/pages/auth/password/email').then(m => m.default || m)

const Settings = () => import('~/pages/settings/index').then(m => m.default || m)
const SettingsProfile = () => import('~/pages/settings/profile').then(m => m.default || m)
const SettingsPassword = () => import('~/pages/settings/password').then(m => m.default || m)

const routes = [
  { path: '/', name: 'index', component: Index },
  { path: '/home', name: 'home', component: Home },

  { path: '/products', name: 'products', component: Products },
  { path: '/product/:id', name: 'product', component: Product },
  { path: '/order', name: 'order', component: Order },
  { path: '/admin', name: 'admin', component: Admin, redirect: { name: 'admin.products' },
    children: [
      { path: 'products', name: 'admin.products', component: AdminProducts },
      { path: 'product/edit/:id', name: 'product.edit', component: AdminEditProduct },
      { path: 'create/product', name: 'admin.createProduct', component: AdminCreateProduct },
      { path: 'orders', name: 'admin.orders', component: AdminOrders },
    ]
  },

  { path: '/login', name: 'login', component: Login },
  { path: '/register', name: 'register', component: Register },
  { path: '/password/reset', name: 'password.request', component: PasswordRequest },
  { path: '/password/reset/:token', name: 'password.reset', component: PasswordReset },

  { path: '/settings',
    component: Settings,
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: SettingsProfile },
      { path: 'password', name: 'settings.password', component: SettingsPassword }
    ] }
]

export function createRouter () {
  return new Router({
    routes,
    scrollBehavior,
    mode: 'history'
  })
}
